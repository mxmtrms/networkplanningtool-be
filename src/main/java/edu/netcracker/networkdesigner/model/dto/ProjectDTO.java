package edu.netcracker.networkdesigner.model.dto;

import edu.netcracker.networkdesigner.enums.ProjectType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDTO {

    private String name;

    private String description;

    private ProjectType type;

}
