package edu.netcracker.networkdesigner.parser;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opendaylight.yangtools.yang.model.api.Module;
import org.opendaylight.yangtools.yang.parser.rfc7950.stmt.AbstractEffectiveDocumentedDataNodeContainer;
import org.opendaylight.yangtools.yang.parser.rfc7950.stmt.container.ContainerEffectiveStatementImpl;
import org.opendaylight.yangtools.yang.parser.rfc7950.stmt.enum_.EnumEffectiveStatementImpl;
import org.opendaylight.yangtools.yang.parser.rfc7950.stmt.leaf.LeafEffectiveStatementImpl;
import org.opendaylight.yangtools.yang.parser.rfc7950.stmt.list.ListEffectiveStatementImpl;
import org.opendaylight.yangtools.yang.parser.rfc7950.stmt.type.EnumSpecificationEffectiveStatement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class YangUtils {

  public static List<ContainerEffectiveStatementImpl> getAllContainersFromFirstLevelOfModule(
          Module module) {
    List<ContainerEffectiveStatementImpl> result = new ArrayList<>();
    module.getChildNodes().stream().filter(
            dataSchemaNode -> dataSchemaNode.getClass().equals(ContainerEffectiveStatementImpl.class))
            .forEach(dataSchemaNode -> result.add((ContainerEffectiveStatementImpl) dataSchemaNode));
    return result;
  }

  public static List<LeafEffectiveStatementImpl> getAllLeafsFromFirstLevelOfModule(Module module) {
    List<LeafEffectiveStatementImpl> result = new ArrayList<>();
    module.getChildNodes().stream().filter(
            dataSchemaNode -> dataSchemaNode.getClass().equals(LeafEffectiveStatementImpl.class))
            .forEach(dataSchemaNode -> result.add((LeafEffectiveStatementImpl) dataSchemaNode));
    return result;
  }

  public static List<ListEffectiveStatementImpl> getAllListsFromContainers(List<ContainerEffectiveStatementImpl> containers) {
    List<ListEffectiveStatementImpl> result = new ArrayList<>();
    containers.stream()
            .map(AbstractEffectiveDocumentedDataNodeContainer::getChildNodes)
            .flatMap(Collection::stream)
            .filter(
                    dataSchemaNodes -> dataSchemaNodes.getClass().equals(ListEffectiveStatementImpl.class))
            .forEach(dataSchemaNode -> result.add((ListEffectiveStatementImpl) dataSchemaNode));
    return result;
  }

  public static List<LeafEffectiveStatementImpl> getAllLeafsFromContainers(List<ContainerEffectiveStatementImpl> containers ) {
    List<LeafEffectiveStatementImpl> result = new ArrayList<>();
    containers.stream()
            .map(AbstractEffectiveDocumentedDataNodeContainer::getChildNodes)
            .flatMap(Collection::stream)
            .filter(
                    dataSchemaNodes -> dataSchemaNodes.getClass().equals(LeafEffectiveStatementImpl.class))
            .forEach(dataSchemaNode -> result.add((LeafEffectiveStatementImpl) dataSchemaNode));
    return result;
  }

  public static List<ListEffectiveStatementImpl> getAllListsFromList(ListEffectiveStatementImpl list) {
    List<ListEffectiveStatementImpl> allLists = new ArrayList<>();
    List<ListEffectiveStatementImpl> allChildLists = getAllListsFromFirstLevelOfList(list);

    if (allChildLists.isEmpty()) {
      return allLists;
    } else {
      for (ListEffectiveStatementImpl listChild : allChildLists) {
        allLists.addAll(getAllListsFromList(listChild));
      }
    }
    return allChildLists;
  }

  private static List<ListEffectiveStatementImpl> getAllListsFromFirstLevelOfList(
          ListEffectiveStatementImpl list) {
    List<ListEffectiveStatementImpl> result = new ArrayList<>();
    list.getChildNodes().stream().filter(
            dataSchemaNode -> dataSchemaNode.getClass().equals(ListEffectiveStatementImpl.class))
            .forEach(dataSchemaNode -> result.add((ListEffectiveStatementImpl) dataSchemaNode));
    return result;
  }

  public static List<LeafEffectiveStatementImpl> getAllLeafsFromList(
          ListEffectiveStatementImpl listEffectiveStatement) {
    List<LeafEffectiveStatementImpl> result = new ArrayList<>();
    listEffectiveStatement.getChildNodes().stream()
            .filter(
                    dataSchemaNodes -> dataSchemaNodes.getClass().equals(LeafEffectiveStatementImpl.class))
            .forEach(dataSchemaNode -> result.add((LeafEffectiveStatementImpl) dataSchemaNode));

    return result;
  }

  public static List<String> getEnumValuesFromLeaf(
          LeafEffectiveStatementImpl enumLeaf) {
    List<String> values = new ArrayList<>();
    enumLeaf.effectiveSubstatements().stream()
            .filter(
                    enumSpec -> enumSpec.getClass().equals(EnumSpecificationEffectiveStatement.class))
            .forEach(enumSpec -> ((EnumSpecificationEffectiveStatement) enumSpec).effectiveSubstatements().stream()
                    .filter(
                            dataSchemaNode -> dataSchemaNode.getClass().equals(EnumEffectiveStatementImpl.class))
                    .forEach(dataSchemaNode -> values.add(
                            ((EnumEffectiveStatementImpl) dataSchemaNode).getName()
                    )));
    return values;
  }

  public static JSONArray getJsonRepresentationForModule(
          Module module) {

    JSONArray jsonRepresentationOfModule = new JSONArray();

    List<ContainerEffectiveStatementImpl> containersFromFirstLevelOfModule
            = YangUtils.getAllContainersFromFirstLevelOfModule(module);
    List<LeafEffectiveStatementImpl> leafsFromFirstLevelOfModule
            = YangUtils.getAllLeafsFromFirstLevelOfModule(module);
    List<ListEffectiveStatementImpl> listsFromFirstLevelContainers
            = YangUtils.getAllListsFromContainers(containersFromFirstLevelOfModule);
    List<LeafEffectiveStatementImpl> leafsFromFirstLevelContainers
            = YangUtils.getAllLeafsFromContainers(containersFromFirstLevelOfModule);

    List<LeafEffectiveStatementImpl> leafsFromFirstLevelOfModuleAndFirstContainer
            = new ArrayList<>(leafsFromFirstLevelOfModule);
    leafsFromFirstLevelOfModuleAndFirstContainer.addAll(
            leafsFromFirstLevelContainers);
    for(LeafEffectiveStatementImpl leaf:
            leafsFromFirstLevelOfModuleAndFirstContainer)
      jsonRepresentationOfModule.put(getLeafJsonRepresentation(leaf));

    for(Object object:
            getJsonRepresentationForLeafListTree(listsFromFirstLevelContainers)) {
      jsonRepresentationOfModule.put(object);
    }

    return jsonRepresentationOfModule;
  }

  private static JSONArray getJsonRepresentationForLeafListTree(
          List<ListEffectiveStatementImpl> listsFromFirstLevelContainers) {
    JSONArray leafListTree = new JSONArray();

    for (ListEffectiveStatementImpl list:
            listsFromFirstLevelContainers) {
      leafListTree.put(getListJsonRepresentation(list));
    }
    return leafListTree;
  }

  private static JSONObject getListJsonRepresentation(ListEffectiveStatementImpl list) {
    JSONArray leafValues = new JSONArray();
    JSONObject listJsonRepresentation = new JSONObject();

    listJsonRepresentation.put("name", list.argument().getLocalName());
    listJsonRepresentation.put("data-type", "list");

    for(LeafEffectiveStatementImpl currentLeaf:
            YangUtils.getAllLeafsFromList(list)) {
      leafValues.put(getLeafJsonRepresentation(currentLeaf));
    }
    listJsonRepresentation.put("leafs", leafValues);

    List<ListEffectiveStatementImpl> listsFromList = YangUtils.getAllListsFromList(list);
    if(!listsFromList.isEmpty()) {
      for (ListEffectiveStatementImpl currentList :
              listsFromList) {
        listJsonRepresentation.accumulate("leafs", getListJsonRepresentation(currentList));
      }
    }

    return listJsonRepresentation;
  }

  private static JSONObject getLeafJsonRepresentation(LeafEffectiveStatementImpl leaf) {
    JSONObject leafJsonRepresentation = new JSONObject();
    leafJsonRepresentation.put("name", leaf.argument().getLocalName());
    leafJsonRepresentation.put("data-type", leaf.getType().getQName().getLocalName());
    if (leaf.getType().getQName().getLocalName().equals("enumeration")) {
      leafJsonRepresentation.put("enum-values", new JSONArray(YangUtils.getEnumValuesFromLeaf(leaf)));
    }
    return leafJsonRepresentation;
  }
}
