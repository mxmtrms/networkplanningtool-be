package edu.netcracker.networkdesigner.parser;

import com.google.common.io.ByteSource;
import org.opendaylight.yangtools.yang.model.api.SchemaContext;
import org.opendaylight.yangtools.yang.model.parser.api.YangParser;
import org.opendaylight.yangtools.yang.model.parser.api.YangParserException;
import org.opendaylight.yangtools.yang.model.parser.api.YangSyntaxErrorException;
import org.opendaylight.yangtools.yang.model.repo.api.SchemaSourceRepresentation;
import org.opendaylight.yangtools.yang.model.repo.api.StatementParserMode;
import org.opendaylight.yangtools.yang.model.api.Module;
import org.opendaylight.yangtools.yang.model.repo.api.YangTextSchemaSource;
import org.opendaylight.yangtools.yang.parser.impl.YangParserFactoryImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class YangModelParser {

  private static final StatementParserMode parserMode = StatementParserMode.DEFAULT_MODE;

  private SchemaContext schemaContext;

  public YangModelParser(Path path) {
    loadSchemaContext(path);
  }

  private File getYangFile(Path path) {
    return new File(String.valueOf(path));
  }

  private SchemaContext parseSchemaYangSource(Path path) {
    YangTextSchemaSource schemaSource = null;
    File yangFile = getYangFile(path);

    try {
      byte[] fileBody = Files.readAllBytes(yangFile.toPath());
      schemaSource =
              YangTextSchemaSource.delegateForByteSource(
                      yangFile.getAbsolutePath(), ByteSource.wrap(fileBody));
    } catch (IOException e) {
      throw new RuntimeException("Can`t read bytes from file." + e);
    }

    return parseSources(schemaSource);
  }

  private SchemaContext parseSources(SchemaSourceRepresentation schemaSource) {
    final YangParser parser = new YangParserFactoryImpl().createParser(parserMode);
    try {
      parser.addSources(schemaSource);
    } catch (IOException | YangSyntaxErrorException e) {
      throw new RuntimeException("Can`t add SchemaSource." + e);
    }
    SchemaContext result = null;
    try {
      result = parser.buildSchemaContext();
    } catch (YangParserException e) {
      throw new RuntimeException("Can`t build SchemaContext" + e);
    }

    return result;
  }

  private void loadSchemaContext(Path path) {
      schemaContext = parseSchemaYangSource(path);
  }

  public Module findModuleByName(String moduleName) {
    return schemaContext.findModule(moduleName)
        .orElseThrow(() -> new RuntimeException(
            String.format("Module with name: %s does not exist", moduleName)));
  }
}

