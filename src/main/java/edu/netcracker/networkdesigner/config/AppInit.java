package edu.netcracker.networkdesigner.config;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.Sheet;
import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.database.repository.ProjectRepository;
import edu.netcracker.networkdesigner.database.repository.SheetRepository;
import edu.netcracker.networkdesigner.database.repository.UserRepository;
import edu.netcracker.networkdesigner.enums.ProjectStatus;
import edu.netcracker.networkdesigner.enums.ProjectType;
import edu.netcracker.networkdesigner.model.dto.ProjectDTO;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.service.common.StorageService;
import edu.netcracker.networkdesigner.service.common.YangStorageService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class AppInit implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private SheetRepository sheetRepository;

    @Autowired
    private YangStorageService yangStorageService;

    @Autowired
    private StorageService storageService;

    @Override
    public void run(ApplicationArguments args) {

        List<Project> projects = new ArrayList<>();
        List<User> users = new ArrayList<>();
        List<Sheet> sheets = new ArrayList<>();

        addUser(users, "Maxim", "Termosa", "maxim",
                "maxims_password", "maxim@gmail.com", "+70000000000");
        addUser(users, "Marina", "Takhteeva", "marina",
                "marinas_password", "marina@gmail.com", "+70000000000");
        addUser(users, "Mikhail", "Mikheev", "mikhail",
                "mikhails_password", "mikhail@gmail.com", "+70000000000");
        userRepository.saveAll(users);

        addProject(projects, 1L, "moscow-network", "Network Plan in Moscow City",
                ProjectStatus.DRAFT, ProjectType.PRIVATE, Arrays.asList(userRepository.getOne(2L), userRepository.getOne(3L)));
        addProject(projects, 3L, "netcracker-network", "Network of Netcracker",
                ProjectStatus.DRAFT, ProjectType.PUBLIC, new ArrayList<>());
        projectRepository.saveAll(projects);

        addSheet(sheets, "First Sheet", projectRepository.getOne(1L));
        addSheet(sheets, "Second Sheet", projectRepository.getOne(1L));
        addSheet(sheets, "First Sheet", projectRepository.getOne(2L));
        addSheet(sheets, "Second Sheet", projectRepository.getOne(2L));
        sheetRepository.saveAll(sheets);

        yangStorageService.init();
    }

    private void addUser(List<User> users, String name, String surname, String username, String password, String email, String phone) {
        if (!userRepository.existsByUsername(username))
            users.add(new User(new UserDTO(username, password, name, surname, email, phone)));
    }

    private void addProject(List<Project> projects, Long user_id, String name,
                            String description, ProjectStatus status, ProjectType type, List<User> members) {
        if (!projectRepository.existsByName(name)) {
            Project project = new Project(new ProjectDTO(name, description, type), userRepository.findById(user_id).orElse(null));
            project.setStatus(status);
            project.setMembers(members);
            projects.add(project);
        }
    }

    private void addSheet(List<Sheet> sheets, String name, Project project){
        if(!sheetRepository.existsByName(name)) {
            String viewId = storageService.saveDocument("views", new Document());
            Sheet sheet = new Sheet(new SheetDTO(name), project, viewId);
            sheets.add(sheet);
        }
    }
}
