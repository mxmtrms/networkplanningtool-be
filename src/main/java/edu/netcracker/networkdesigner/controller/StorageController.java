package edu.netcracker.networkdesigner.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.netcracker.networkdesigner.service.common.StorageService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RequestMapping("/storage")
@RestController
public class StorageController {

    @Autowired
    private StorageService storageService;

    @GetMapping("/{collectionName}/{id}")
    public ResponseEntity<String> getById(
            @PathVariable String collectionName,
            @PathVariable String id) throws JsonProcessingException {
        return ResponseEntity.ok().body(storageService.getDocumentByIdAndCollection(collectionName, id));
    }

    @PostMapping("/{collectionName}")
    public ResponseEntity<String> save(
            @PathVariable String collectionName,
            @RequestBody Document body) {
        String id = storageService.saveDocument(collectionName, body);
        URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/models/storage/")
                .path(collectionName)
                .query("id={id}")
                .build(id);
        return ResponseEntity.created(uri).body(id);
    }

    @GetMapping("/{collectionName}")
    public ResponseEntity<String> getAll(
            @PathVariable String collectionName) throws JsonProcessingException {
        return ResponseEntity.ok().body(storageService.getAllDocuments(collectionName));
    }

    @DeleteMapping("/{collectionName}/{id}")
    public ResponseEntity<String> deleteById(
            @PathVariable String collectionName,
            @PathVariable String id) {
        storageService.deleteDocumentByIdAndCollection(collectionName, id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{collectionName}/{id}")
    public ResponseEntity<String> updateById(
            @PathVariable String collectionName,
            @PathVariable String id,
            @RequestBody Document body) throws JsonProcessingException {
        return ResponseEntity.ok().body(storageService.updateDocumentByIdAndCollection(collectionName, id, body));
    }
}