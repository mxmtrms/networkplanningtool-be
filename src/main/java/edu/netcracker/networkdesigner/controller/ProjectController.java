package edu.netcracker.networkdesigner.controller;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.model.dto.ProjectDTO;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;
import edu.netcracker.networkdesigner.service.database.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @PostMapping()
    public Project addProject(@RequestBody ProjectDTO dto,
                              @AuthenticationPrincipal User owner) {
        return projectService.addProject(dto, owner);
    }

    @PostMapping("/create")
    public Project addProject(@RequestBody ProjectDTO dto,
                              @RequestParam(name = "ownerId") Long ownerId) {
        return projectService.addProject(dto, ownerId);
    }

    @GetMapping()
    public List<Project> getAllProjects() {
        return projectService.getAllProjects();
    }

    @PatchMapping("/{projectId}")
    public Project updateProject(@PathVariable(value = "projectId") Long projectId,
                                 @RequestBody ProjectDTO projectDTO) {
        return projectService.updateProject(projectId, projectDTO);
    }

    @GetMapping("/{projectId}")
    public Project getProjectById(@PathVariable(value = "projectId") Long projectId) {
        return projectService.getProjectById(projectId);
    }

    @PostMapping("/{projectId}/members")
    public Project addPrivateUser(@RequestParam(value = "userId") Long userId,
                                  @PathVariable(value = "projectId") Long projectId) {
        return projectService.addPrivateUser(userId, projectId);
    }

    @GetMapping("/{projectId}/members")
    public List<User> getProjectMembers(@PathVariable(value = "projectId") Long projectId) {
        return projectService.getProjectMembers(projectId);
    }

    @DeleteMapping("/{projectId}/members/{userId}")
    public void deleteMember(
            @PathVariable(value = "projectId") Long projectId,
            @PathVariable(value = "userId") Long userId) {
        projectService.deleteMember(projectId, userId);
    }

    @DeleteMapping("/{projectId}")
    public void deleteProject(@PathVariable(value = "projectId") Long projectId) {
        projectService.deleteProject(projectId);
    }

    @PostMapping("/{projectId}/sheets")
    public Project addSheetToProject(
            @PathVariable(value = "projectId") Long projectId,
            @RequestBody SheetDTO sheetDTO) {
        return projectService.addSheet(sheetDTO, projectId);
    }
}