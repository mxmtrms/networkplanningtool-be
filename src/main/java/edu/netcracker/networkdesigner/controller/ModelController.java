package edu.netcracker.networkdesigner.controller;

import edu.netcracker.networkdesigner.database.entity.ModelInfo;
import edu.netcracker.networkdesigner.enums.ModelType;
import edu.netcracker.networkdesigner.exception.ModelNotFoundException;
import edu.netcracker.networkdesigner.service.database.ModelService;
import edu.netcracker.networkdesigner.service.common.YangStorageService;
import edu.netcracker.networkdesigner.service.common.YangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/models")
public class ModelController {

    @Autowired
    private YangStorageService yangStorageService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private YangService yangService;

    @PostMapping()
    public ResponseEntity<String> uploadFile(@RequestParam("type") String type,
                                     @RequestParam("file") MultipartFile file){
        String fileDownloadUri
                = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/models/")
                .path(yangStorageService.store(file, ModelType.convert(type)).toString())
                .path("/export")
                .toUriString();

        return ResponseEntity.ok(fileDownloadUri);
    }

    @GetMapping("/{infoId}/export")
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@PathVariable Long infoId) throws ModelNotFoundException {
        Resource file = yangStorageService.load(infoId);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping()
    public List<ModelInfo> getAllModelHistory() {
        return modelService.getAll();
    }

    @GetMapping("/{type}")
    public String getJsonRepresentationOfModule(@PathVariable String type) throws ModelNotFoundException {
        return yangService.getJsonYangModelByType(type);
    }

    @GetMapping("/{infoId}/content")
    public String getContentOfYangFile(@PathVariable Long infoId) throws ModelNotFoundException, IOException {
        return yangStorageService.getContent(infoId);
    }
}
