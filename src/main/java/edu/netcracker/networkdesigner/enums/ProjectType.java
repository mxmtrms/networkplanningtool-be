package edu.netcracker.networkdesigner.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ProjectType {
    PRIVATE("private"),
    PUBLIC("public");

    private final String access;

    ProjectType(String access) {
        this.access = access;
    }

    @JsonValue
    public String getAccess() {
        return access;
    }
}
