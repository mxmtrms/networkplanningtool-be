package edu.netcracker.networkdesigner.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ModelType {

    LOCATION("location"),
    ROUTER("router"),
    FIREWALL("firewall"),
    SWITCH("switch"),
    PC("pc"),
    SERVER("server"),
    CLOUD("cloud"),
    HUB("hub"),
    LINK("link");

    private final String type;

    ModelType(String type) {
        this.type = type;
    }

    public static ModelType convert(String type) {
        switch (type) {
            case "location":
                return ModelType.LOCATION;
            case "router":
                return ModelType.ROUTER;
            case "firewall":
                return ModelType.FIREWALL;
            case "switch":
                return ModelType.SWITCH;
            case "pc":
                return ModelType.PC;
            case "server":
                return ModelType.SERVER;
            case "cloud":
                return ModelType.CLOUD;
            case "link":
                return ModelType.LINK;
            case "hub":
                return ModelType.HUB;
            default:
                throw new RuntimeException("No such type");
        }
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
