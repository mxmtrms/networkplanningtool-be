package edu.netcracker.networkdesigner.utils;

import com.google.common.io.Files;
import java.util.Optional;

public final class Utils {

    private static String getExtensionFromFilename(String fileName) {
        return Optional.ofNullable(fileName)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(fileName.lastIndexOf(".") + 1))
                .orElse("");
    }

    public static String getNameWithTimeStamp(String fileNameWithExt, Long createdAt) {
        String extension = getExtensionFromFilename(fileNameWithExt);
        String fileNameWithOutExt = Files.getNameWithoutExtension(fileNameWithExt);

        return fileNameWithOutExt + '-' + createdAt + '.' + extension;
    }
}
