package edu.netcracker.networkdesigner.service.database;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.Sheet;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;

import java.util.List;

public interface SheetService {

    Sheet addSheet(SheetDTO sheet, Project project);

    void deleteSheet(Long id);

    Sheet updateSheet(SheetDTO sheet, Long id);

    Sheet getSheetById(Long id);

    List<Sheet> getAllSheets();
}
