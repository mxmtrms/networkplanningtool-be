package edu.netcracker.networkdesigner.service.database;

import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.payload.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;
import java.util.UUID;

public interface UserService extends UserDetailsService {

    User getUserById(Long id);

    User updateUser(Long id, UserDTO userDTO);

    User addUser(UserDTO dto);

    ResponseEntity<ApiResponse> activate(UUID uuid) throws UserPrincipalNotFoundException;

    List<User> getAllUsers();
}
