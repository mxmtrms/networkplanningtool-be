package edu.netcracker.networkdesigner.service.database.impl;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.Sheet;
import edu.netcracker.networkdesigner.database.repository.SheetRepository;
import edu.netcracker.networkdesigner.exception.ResourceNotFoundException;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;
import edu.netcracker.networkdesigner.service.common.StorageService;
import edu.netcracker.networkdesigner.service.database.SheetService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SheetServiceImpl implements SheetService {

    @Autowired
    private SheetRepository sheetRepository;

    @Autowired
    private StorageService storageService;

    @Override
    public Sheet addSheet(SheetDTO sheetDTO, Project project) {
        String viewId = storageService.saveDocument("views", new Document());
        Sheet sheet = new Sheet(sheetDTO, project, viewId);
        return sheetRepository.save(sheet);
    }

    @Override
    public void deleteSheet(Long id) {
        Sheet sheet = getSheetById(id);
        sheetRepository.delete(sheet);
    }

    @Override
    public Sheet updateSheet(SheetDTO dto, Long id) {
        Sheet sheet = getSheetById(id);
        sheet.setName(dto.getName());
        return sheetRepository.save(sheet);
    }

    @Override
    public Sheet getSheetById(Long id) {
        return sheetRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Sheet",
                        "id", String.valueOf(id)));
    }

    @Override
    public List<Sheet> getAllSheets(){
        return sheetRepository.findAll();
    }
}
