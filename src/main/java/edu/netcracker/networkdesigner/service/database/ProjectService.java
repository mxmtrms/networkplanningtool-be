package edu.netcracker.networkdesigner.service.database;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.model.dto.ProjectDTO;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;

import java.util.List;

public interface ProjectService {

    Project getProjectById(Long id);

    List<Project> getAllProjects();

    Project addProject(ProjectDTO projectDTO, User owner);

    Project addProject(ProjectDTO projectDTO, Long ownerId);

    void deleteProject(Long id);

    Project addPrivateUser(Long userId, Long projectId);

    List<User> getProjectMembers(Long projectId);

    List<Project> getAvailableProjectsByUserId(Long userId);

    List<Project> getAvailableProjectsByUser(User user);

    Project updateProject(Long projectId, ProjectDTO projectDTO);

    Project addSheet(SheetDTO sheetDTO, Long projectId);

    Project deleteMember(Long projectId, Long userId);
}
