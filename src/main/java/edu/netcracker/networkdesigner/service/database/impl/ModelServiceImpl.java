package edu.netcracker.networkdesigner.service.database.impl;

import edu.netcracker.networkdesigner.database.entity.ModelInfo;
import edu.netcracker.networkdesigner.database.repository.ModelInfoRepository;
import edu.netcracker.networkdesigner.exception.ModelNotFoundException;
import edu.netcracker.networkdesigner.service.database.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    @Autowired
    private ModelInfoRepository modelInfoRepository;

    @Override
    public ModelInfo addInfo(ModelInfo modelInfo) {
        return modelInfoRepository.save(modelInfo);
    }

    @Override
    public List<ModelInfo> getAll() { return modelInfoRepository.findAll(); }

    @Override
    public ModelInfo getModelInfoById(Long infoId) throws ModelNotFoundException {
        return modelInfoRepository.findById(infoId).orElseThrow(
                () -> new ModelNotFoundException("ModelInfo object with id=(" + infoId.toString() + ") was not found"));
    }
}
