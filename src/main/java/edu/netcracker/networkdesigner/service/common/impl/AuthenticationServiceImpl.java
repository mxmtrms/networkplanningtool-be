package edu.netcracker.networkdesigner.service.common.impl;

import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.database.repository.UserRepository;
import edu.netcracker.networkdesigner.exception.AppException;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.payload.ApiResponse;
import edu.netcracker.networkdesigner.payload.JwtAuthenticationResponse;
import edu.netcracker.networkdesigner.payload.LoginRequest;
import edu.netcracker.networkdesigner.security.JwtTokenProvider;
import edu.netcracker.networkdesigner.service.common.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private MailSenderService senderService;

    @Autowired
    private UserRepository userRepository;

    @Value("${server.address}")
    private String apiHost;

    @Value("${server.port}")
    private String apiPort;

    @Override
    public ResponseEntity<JwtAuthenticationResponse> authenticate(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @Override
    public ResponseEntity<ApiResponse> registration(UserDTO signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        User user = new User(signUpRequest);
        User result = userRepository.save(user);

        try {
            senderService.send(user.getEmail(), "Network Planinng Tool Activation", "Link for activation: http://" + apiHost + ":" + apiPort + "/auth/activation/" + user.getActivationCode());
        } catch (Exception e) {
            throw new AppException("Some problems with sending verification email. Check your email.");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{userId}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }
}
