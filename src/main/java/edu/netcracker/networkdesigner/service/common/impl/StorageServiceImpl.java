package edu.netcracker.networkdesigner.service.common.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.DBObject;
import edu.netcracker.networkdesigner.service.common.StorageService;
import edu.netcracker.networkdesigner.utils.ObjectIdSerializerModule;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String getDocumentByIdAndCollection(String collectionName, String id) throws JsonProcessingException {
        List<DBObject> docs = mongoTemplate.find(
                new Query(Criteria.where("_id").is(id)), DBObject.class, collectionName);
        return getMapper().writeValueAsString(docs.get(0));
    }

    @Override
    public String saveDocument(String collectionName, Document body) {
        mongoTemplate.save(body, collectionName);
        return body.get("_id").toString();
    }

    @Override
    public String getAllDocuments(String collectionName) throws JsonProcessingException {
        List<DBObject> docs = mongoTemplate.findAll(DBObject.class, collectionName);
        return getMapper().writeValueAsString(docs);
    }

    @Override
    public void deleteDocumentByIdAndCollection(String collectionName, String id) {
        mongoTemplate.findAndRemove(
                new Query(Criteria.where("_id").is(id)), DBObject.class, collectionName);
    }

    @Override
    public String updateDocumentByIdAndCollection(String collectionName, String id, Document doc) throws JsonProcessingException {
        Document replacement = mongoTemplate.findAndReplace(
                new Query(Criteria.where("_id").is(id)), doc, (new FindAndReplaceOptions()).returnNew(), collectionName);
        return getMapper().writeValueAsString(replacement);
    }

    private ObjectMapper getMapper() {
        return new ObjectMapper().registerModule(new ObjectIdSerializerModule());
    }
}
