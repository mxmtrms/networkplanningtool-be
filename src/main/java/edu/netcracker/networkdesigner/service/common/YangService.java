package edu.netcracker.networkdesigner.service.common;

import edu.netcracker.networkdesigner.exception.ModelNotFoundException;

public interface YangService {

    String getJsonYangModelByType(String type) throws ModelNotFoundException;
}
