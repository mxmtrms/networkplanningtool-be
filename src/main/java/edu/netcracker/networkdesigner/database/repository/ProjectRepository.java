package edu.netcracker.networkdesigner.database.repository;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.enums.ProjectType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    List<Project> getProjectsByType(ProjectType projectType);

    boolean existsByName(String username);

    Optional<Project> findById(Long id);
}
