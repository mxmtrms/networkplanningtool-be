package edu.netcracker.networkdesigner.database.repository;

import edu.netcracker.networkdesigner.database.entity.Sheet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SheetRepository extends JpaRepository<Sheet, Long> {

    Sheet getSheetById(Long id);

    boolean existsByName(String name);
}
