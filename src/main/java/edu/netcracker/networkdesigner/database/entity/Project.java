package edu.netcracker.networkdesigner.database.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.netcracker.networkdesigner.enums.ProjectStatus;
import edu.netcracker.networkdesigner.enums.ProjectType;
import edu.netcracker.networkdesigner.model.audit.DateAudit;
import edu.netcracker.networkdesigner.model.dto.ProjectDTO;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
@Table(name = "projects")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Project extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    @Enumerated(value = EnumType.STRING)
    private ProjectStatus status;

    @Column
    @Enumerated(value = EnumType.STRING)
    private ProjectType type;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "owner_id")
    private User owner;

    @OneToMany(mappedBy = "project")
    private List<Sheet> sheets;

    @ManyToMany()
    @JoinTable(name = "usrs_projects",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> members = new ArrayList<>();

    public Project(ProjectDTO dto, User owner) {
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.status = ProjectStatus.DRAFT;
        this.type = dto.getType();
        this.owner = owner;
    }

}
