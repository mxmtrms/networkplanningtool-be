package edu.netcracker.networkdesigner.database.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import edu.netcracker.networkdesigner.enums.ModelType;
import edu.netcracker.networkdesigner.model.audit.DateAudit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.Instant;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "model_info")
@NoArgsConstructor
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class
)
public class ModelInfo extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "type")
    @Enumerated(value = EnumType.STRING)
    private ModelType type;

    @Column
    private Long createdAt;

    @Column(name = "created_by")
    private String username;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private String jsonRepresentation;

    public ModelInfo(MultipartFile file, ModelType type) {
        this.type = type;
        this.fileName = StringUtils.cleanPath(file.getOriginalFilename());
        this.username = SecurityContextHolder.getContext().getAuthentication().getPrincipal() == "anonymousUser" ?
                "anonymousUser": ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        this.createdAt = Instant.now().toEpochMilli();
    }
}
