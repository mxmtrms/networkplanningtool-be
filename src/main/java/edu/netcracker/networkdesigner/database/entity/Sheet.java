package edu.netcracker.networkdesigner.database.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Data
@Entity
@Table(name = "sheets")
@NoArgsConstructor
public class Sheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column(name = "view_id")
    private String viewId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "project_id")
    private Project project;

    public Sheet(SheetDTO sheet, Project project, String viewId){
        this.name = sheet.getName();
        this.project = project;
        this.viewId = viewId;
    }
}
