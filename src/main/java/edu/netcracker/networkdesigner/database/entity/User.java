package edu.netcracker.networkdesigner.database.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name = "usrs")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler",
        "activationCode", "active", "enabled",
        "credentialsNonExpired", "accountNonExpired",
        "authorities", "accountNonLocked", "password"})
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String password;

    @Column
    private String username;

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private String email;

    @Column
    private String phone;

    @Column(name = "active")
    private boolean isActive;

    @Column
    private UUID activationCode;

    @JsonIgnore
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<Project> personalProjects;

    @JsonIgnore
    @ManyToMany(mappedBy = "members", fetch = FetchType.LAZY)
    private List<Project> projects;

    public User(UserDTO userDTO) {
        this.username = userDTO.getUsername();
        this.password = encode(userDTO.getPassword());
        this.name = userDTO.getName();
        this.surname = userDTO.getSurname();
        this.email = userDTO.getEmail();
        this.phone = userDTO.getPhone();
        this.activationCode = UUID.randomUUID();
        this.isActive = false;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<GrantedAuthority>(Collections.singleton(new SimpleGrantedAuthority("USER")));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    private String encode(String password) {
        return (new BCryptPasswordEncoder()).encode(password);
    }
}
